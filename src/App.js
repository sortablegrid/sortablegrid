import React from "react";
import { ThemeProvider } from "@material-ui/core";

import "./app/ui/configs/BASIC_STYLE.css";
import theme from "./app/ui/configs/THEME";

import Routing from "./app/ui/routing/Routing";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Routing />
    </ThemeProvider>
  );
}

export default App;
