import React, { Component } from "react";

import { withStyles } from "@material-ui/core";

import useStyles from "./MeetingGrid.style";
import MeetingCard from "../../meeting/MeetingCard";
import { Grid, IconButton } from "@material-ui/core";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const useStyles = (theme) => ({});

export default useStyles;

class GridColumn extends Component {
  render() {
    const {
      col: { list, id },
    } = this.props;

    return (
      <Droppable droppableId={id}>
        {(provided) => (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <h2>{id}</h2>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                minHeight: "120px",
              }}
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {list.map((text, index) => (
                <GridItem key={text} text={text} index={index} />
              ))}
              {provided.placeholder}
            </div>
          </div>
        )}
      </Droppable>
    );
  }
}

export default withStyles(useStyles)(MeetingGrid);
