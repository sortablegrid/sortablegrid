const useStyles = (theme) => ({
  container: {
    // backgroundImage: `url(${img})`,
    backgroundColor: "#cccccc",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "100%",
    width: "100%",
    position: "relative",
  },
});

export default useStyles;
