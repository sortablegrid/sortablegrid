import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, Menu, MenuItem } from "@material-ui/core";

import useStyles from "./MoreMenu.style";
import { IconButton } from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";

class MoreMenu extends Component {
  state = { anchorEl: null };

  handleClick = (event) => {
    const newState = { ...this.state };
    newState["anchorEl"] = event.currentTarget;
    this.setState(newState);
  };

  handleClose = () => {
    const newState = { ...this.state };
    newState["anchorEl"] = null;
    this.setState(newState);
  };

  render() {
    const { classes, items } = this.props;

    return (
      <div>
        <IconButton
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <MoreVert />
        </IconButton>

        {items.map((menuItem) => (
          <Menu
            id="simple-menu"
            anchorEl={this.state.anchorEl}
            keepMounted
            open={Boolean(this.state.anchorEl)}
            onClose={this.handleClose}
          >
            <MenuItem onClick={menuItem.onClick}>{menuItem.title}</MenuItem>
          </Menu>
        ))}
      </div>
    );
  }
}

MoreMenu.propTypes = {
  items: PropTypes.array,
};

export default withStyles(useStyles)(MoreMenu);
