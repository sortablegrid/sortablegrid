import React, { Component } from "react";

import { withStyles, Grid } from "@material-ui/core";

import useStyles from "./SortableGrid.style";

import { DragDropContext } from "react-beautiful-dnd";
import GridColumn from "./GridColumn";

// TODO: dynamic image

class SortableGrid extends Component {
  render() {
    const {
      classes,
      columns,
      // onDelete,
      colSpan,
      gridDimension,
      onDragEnd,
    } = this.props;

    return (
      <DragDropContext onDragEnd={onDragEnd}>
        <Grid
          container
          spacing={2}
          style={{ margin: "auto", width: gridDimension.width }}
        >
          {columns.map((col) => (
            <Grid item lg={colSpan} xl={colSpan} md={6} xs={12} sm={6} >
              <GridColumn itemHeight={gridDimension.height} col={col} key={col.id} />
            </Grid>
          ))}
        </Grid>
      </DragDropContext>
    );
  }
}

export default withStyles(useStyles)(SortableGrid);
