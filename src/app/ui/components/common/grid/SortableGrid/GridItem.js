import React, { Component } from "react";

import { withStyles } from "@material-ui/core";

// import useStyles from "./SortableGrid.style";
// import MeetingCard from "../../../meeting/MeetingCard";
import { Grid, IconButton } from "@material-ui/core";
import { Droppable, Draggable } from "react-beautiful-dnd";
import MeetingCard from "./../../../meeting/MeetingCard/index";

const useStyles = (theme) => ({});

class GridItem extends Component {
  render() {
    const { text, index, height } = this.props;

    return (
      <Draggable draggableId={text} index={index}>
        {(provided) => (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <MeetingCard name={text} height={height}/>
          </div>
        )}
      </Draggable>
    );
  }
}

export default withStyles(useStyles)(GridItem);
