import React, { Component } from "react";

import { withStyles } from "@material-ui/core";

import { Droppable } from "react-beautiful-dnd";
import GridItem from "./GridItem";

const useStyles = (theme) => ({});

class GridColumn extends Component {
  render() {
    const {
      col: { list, id }, itemHeight
    } = this.props;

    return (
      <Droppable droppableId={id}>
        {(provided) => (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
            }}
            {...provided.droppableProps}
            ref={provided.innerRef}
          >

            {list.map((item, index) => (
              <GridItem height={itemHeight} key={item.name} text={item.name} index={index} />
            ))}
            <div style={{ border: "1px dashed #fafafa" }}>
              {provided.placeholder}
            </div>
          </div>
        )}
      </Droppable>
    );
  }
}

export default withStyles(useStyles)(GridColumn);
