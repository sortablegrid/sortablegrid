import React, { Component } from "react";

import { withStyles } from "@material-ui/core";

import useStyles from "./MeetingGrid.style";
import MeetingCard from "../../../components/meeting/MeetingCard";
import { Grid, IconButton } from "@material-ui/core";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

// TODO: dynamic image

class MeetingGrid extends Component {
  render() {
    const {
      colSpan,
      classes,
      meetingItems,
      onDelete,
      meetingGridDimension,
      onDragEnd,
    } = this.props;

    return (
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="meetingItems">
          {(provided) => (
            <Grid
              className={classes.meetingContainer + " " + "meetingItems"}
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{ width: meetingGridDimension.width }}
              container
              spacing={2}
            >
              {meetingItems.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided) => (
                    <Grid
                      item
                      xs={colSpan}
                      className={classes.meetingItem}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <MeetingCard
                        name={item.name}
                        onDelete={() => onDelete(item.id)}
                      />
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}

// MeetingCard.propTypes = {
//   // image: PropTypes.string,
//   width: PropTypes.string,
//   name: PropTypes.string,
//   onDelete: PropTypes.func,
// };

// MeetingCard.defaultProps = {
//   name: "Nick Butler",
//   width: "100%",

//   // image: true,
// };

export default withStyles(useStyles)(MeetingGrid);
