const useStyles = (theme) => ({
  meetingContainer: {
    margin: "auto",
  },

  meetingItem: {
    padding: theme.spacing(1),
  },
});

export default useStyles;
