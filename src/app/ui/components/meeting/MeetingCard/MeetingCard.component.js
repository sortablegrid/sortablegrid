import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles, Typography, Paper } from "@material-ui/core";

import useStyles from "./MeetingCard.style";
import { IconButton } from "@material-ui/core";
import { KeyboardVoice, VideoCall, Wifi } from "@material-ui/icons";
import MoreMenu from "../../common/menu/MoreMenu";
import theme from "../../../configs/THEME";

class MeetingCard extends Component {
  render() {
    const { name, width, classes, onDelete, height } = this.props;

    return (
      <div className={classes.container} style={{height:height}}>
        <div className={classes.content}>
          <MoreMenu
            items={[{ title: "Delete", onClick: onDelete }]}
            className={classes.more}
          />

          <div className={classes.footerContainer}>
            <div className={classes.toolsContainer}>
              <IconButton
                style={{
                  padding: "4px",
                  fontSize: "1rem",
                  color: theme.palette.error.main,
                }}
              >
                <KeyboardVoice style={{ fontSize: "1rem" }} />
              </IconButton>
              <IconButton
                style={{
                  padding: "4px",
                  fontSize: "1rem",
                  color: theme.palette.error.main,
                }}
                size="small"
              >
                <VideoCall style={{ fontSize: "1rem" }} />
              </IconButton>
            </div>
            <div className={classes.titleContianer}>
              <Typography
                className={classes.title}
                variant="subtitle1"
                component="span"
              >
                {name}
              </Typography>
            </div>
            <div className={classes.wifiContainer}>
              <IconButton
                style={{ color: theme.palette.secondary.main }}
                size="small"
              >
                <Wifi />
              </IconButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MeetingCard.propTypes = {

  name: PropTypes.string,
  onDelete: PropTypes.func,
};



export default withStyles(useStyles)(MeetingCard);
