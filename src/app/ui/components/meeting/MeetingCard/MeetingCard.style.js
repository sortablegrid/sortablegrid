import img from "../../../assets/images/men.jpg";

const useStyles = (theme) => ({
  container: {
    width: "100%",
    padding: theme.spacing(1),
  },

  content: {
    backgroundImage: `url(${img})`,
    backgroundColor: "#cccccc",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "100%",
    position: "relative",
  },

  more: {},

  footerContainer: {
    display: "flex",
    height: "10%",
    position: "absolute",
    width: "100%",
    bottom: theme.spacing(2),
    alignItems: "center",
  },

  titleContianer: {
    textAlign: "center",
    width: "50%",
  },

  title: {
    backgroundColor: "#070707",
    opacity: "0.5",
    borderRadius: theme.spacing(4),
    padding: theme.spacing(1, 3),
    color: "#fafafa",
  },

  toolsContainer: {
    width: "25%",
  },

  wifiContainer: {
    width: "25%",
    textAlign: "center",
  },
});

export default useStyles;
