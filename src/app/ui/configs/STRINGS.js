const STRINGS = {
  app_name: "barbod",
  barbod: "باربد",
  enter: "ورود به برنامه",
  try_again: "تلاش مجدد",
  receive_verification_code: "دریافت کد تایید",
  enter_your_phone_number: "شماره همراه خود را وارد کنید",
};

export default STRINGS;
