import { createMuiTheme } from "@material-ui/core";
import { green, blue, blueGrey, grey, red } from "@material-ui/core/colors";
import { fade } from "@material-ui/core/styles/colorManipulator";

const theme = createMuiTheme({
  spacing: [0, 4, 8, 16, 32, 64],
  palette: {
    secondary: {
      main: green["A700"],
      contrastText: grey["50"],
    },
    primary: {
      main: blue["A700"],
      contrastText: blueGrey["900"],
    },
    text: {
      primary: blueGrey["900"],
      secondary: grey["50"],
    },
    background: {
      main: grey["50"],
    },
    error: {
      main: red["800"],
    },
    surface: {
      main: grey["50"],
    },
    onSurface: {
      main: blueGrey["900"],
      alpha25: fade(blueGrey["900"], 0.25),
    },
  },
  typography: {
    fontFamily: ["iran-sans-fa-number"].join(","),
  },
});

export default theme;
