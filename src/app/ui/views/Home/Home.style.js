import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1, 5),
  },
  headerContainer: {
    display: "flex",
    height: "5%",
  },

  footerContainer: {
    position: "fixed",
    bottom: "0",
    left: "0",
    backgroundColor: "#000000cc",
    marginTop: theme.spacing(3),
    width: "100%",
  },

  toolbox: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: theme.spacing(1, 4),
  },
}));

export default useStyles;
