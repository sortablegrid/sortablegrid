import React, { useState } from "react";
import useStyles from "./Home.style";

import {
  Add,
  Timer,
  AspectRatio,
  KeyboardVoice,
  VideoCall,
  ListAlt,
} from "@material-ui/icons";

import { IconButton, Typography, Badge, List } from "@material-ui/core";
import SortableGrid from "../../components/common/grid/SortableGrid";
import theme from "../../configs/THEME";

let meetingItems = [
  { id: "0", name: "item-0" },
  { id: "1", name: "item-1" },
  { id: "2", name: "item-2" },
  { id: "3", name: "item-3" },
];

const Home = () => {
  const classes = useStyles();

  const meetingGridDimensionList = [
    { colSpan: 1, width: "90%", height: 200 },
    { colSpan: 2, width: "60%", height: 180 },
    { colSpan: 3, width: "80%", height: 160 },
    { colSpan: 4, width: "90%", height: 140 },
  ];

  const [colSpan, setColSpan] = useState(
    window.sessionStorage.getItem("colSpan")
      ? JSON.parse(window.sessionStorage.getItem("colSpan"))
      : 6
  );

  const [meetingGridDimension, setMeetingGridDimension] = useState(
    window.sessionStorage.getItem("meetingGridDimension")
      ? JSON.parse(window.sessionStorage.getItem("meetingGridDimension"))
      : { width: "70%", height: 180 }
  );

  const initialColumns = [
    {
      id: "0",
      list: [
        { id: "0", name: "item-0" },
        { id: "2", name: "item-2" },
      ],
    },
    {
      id: "1",
      list: [
        { id: "1", name: "item-1" },
        { id: "3", name: "item-3" },
      ],
    },
  ];

  const [columns, setColumns] = useState(
    window.sessionStorage.getItem("meetingGridColumns")
      ? JSON.parse(window.sessionStorage.getItem("meetingGridColumns"))
      : initialColumns
  );

  React.useEffect(() => {
    window.sessionStorage.setItem(
      "meetingGridColumns",
      JSON.stringify(columns)
    );
  }, [columns]);

  React.useEffect(() => {
    window.sessionStorage.setItem("colSpan", JSON.stringify(colSpan));
  }, [colSpan]);

  React.useEffect(() => {
    window.sessionStorage.setItem(
      "meetingGridDimension",
      JSON.stringify(meetingGridDimension)
    );
  }, [meetingGridDimension]);

  const handleAddMember = () => {
    if (meetingItems.length < 16) {
      meetingItems.push({
        id: meetingItems.length.toString(),
        name: "item-" + meetingItems.length,
      });

      const newColSpan = Math.ceil(Math.sqrt(meetingItems.length));

      setColSpan(12 / newColSpan);

      const meetingGridDimensionItem = meetingGridDimensionList.find(
        (item) => item.colSpan === newColSpan
      );

      setMeetingGridDimension(
        meetingGridDimensionItem
          ? {
              width: meetingGridDimensionItem.width,
              height: meetingGridDimensionItem.height,
            }
          : { width: "90%", height: 200 }
      );

      makeColumns(newColSpan);
    }
  };

  const makeColumns = (newColSpan) => {
    const newColumns = [...columns];

    const index = meetingItems.length - 1;

    const lastItem = meetingItems[index];

    if (Math.sqrt(meetingItems.length - 1) === newColSpan - 1) {
      newColumns.push({ id: newColumns.length.toString(), list: [lastItem] });
    } else {
      newColumns[index % newColSpan]["list"].push(lastItem);
    }

    setColumns(newColumns);
  };

  const handleOnDragEnd = ({ source, destination }) => {
    if (destination === undefined || destination === null) return null;

    if (
      source.droppableId === destination.droppableId &&
      destination.index === source.index
    )
      return null;

    const start = columns[source.droppableId];
    const end = columns[destination.droppableId];

    if (start === end) {
      const newList = start.list.filter((_, idx) => idx !== source.index);

      newList.splice(destination.index, 0, start.list[source.index]);

      const newColumns = [...columns];

      newColumns[start.id]["list"] = newList;

      setColumns(newColumns);
      return null;
    } else {
      const newStartList = start.list.filter((_, idx) => idx !== source.index);

      const newEndList = end.list;

      newEndList.splice(destination.index, 0, start.list[source.index]);

      const newColumns = [...columns];

      newColumns[start.id]["list"] = newStartList;

      newColumns[end.id]["list"] = newEndList;

      setColumns(newColumns);
      return null;
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.headerContainer}>
        <div style={{ flex: "30%" }}>
          <Typography style={{ color: "#fafafa" }} variant="h6" Component="h6">
            UniClient
          </Typography>
        </div>
        <div style={{ flex: "50%" }}>
          <Typography style={{ color: "#fafafa" }} variant="h6" Component="h6">
            Case Of Kenedy Murder{" "}
            <Timer style={{ fontSize: "1rem", margin: theme.spacing(0, 2) }} />
            <span style={{ color: theme.palette.secondary.main }}>(45:32)</span>
          </Typography>
        </div>
      </div>

      <div className={classes.gridContainer}>
        <SortableGrid
          colSpan={colSpan}
          columns={columns}

          onDragEnd={handleOnDragEnd}
          gridDimension={meetingGridDimension}

        />
      </div>

      <div className={classes.footerContainer}>
        <div className={classes.toolbox}>
          <Badge badgeContent={4} color="error">
            <ListAlt style={{ color: theme.palette.text.secondary }} />
          </Badge>
          <div>
            <IconButton size="large">
              <KeyboardVoice style={{ color: theme.palette.text.secondary }} />
            </IconButton>
            <IconButton
              size="large"
              onClick={handleAddMember}
              style={{ color: theme.palette.error.main }}
            >
              <Add />
            </IconButton>
            <IconButton size="large">
              <VideoCall style={{ color: theme.palette.text.secondary }} />
            </IconButton>
          </div>

          <AspectRatio style={{ color: theme.palette.text.secondary }} />
        </div>
      </div>
    </div>
  );
};

export default Home;
